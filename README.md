## Ważne

1. API podpięte do tego repo to mój codesnadbox (https://codesandbox.io/s/mmomqnmp08). Ogólnie działa ale po jakimś czasie gdy nie maaktywności jest usypiany. Nie wiem czy ktoś poza mną może go obudzić. CodeSandobx oferuje forkowanie więc możecie sobie zrobić tymczasowo swoje API. Przy okazji zachęcam do wysłania kilku doclców na wersję "Patron" bo to super projekt :)

2. Ktoś może się zastanawiać (i faktycznie jest to nieruszony temat): w jaki sposób komponenty, które nie są stroną mogą dostawać dane. Oczywiście nie chcemy pakować wszystkiego w `getInitialProps` bo sporo danych jest potrzebnych gdy są potrzebne a nie od razu :). Temat architektonicznie jest bardzo ciekawy. Można zacząć od zwykłego `fetch` w `componentDidMount`, można używać `HoC` (które w 99% są z dupy i trzeba to wiedzieć). Inna opcja to deklaratywne komponenty do wykonywania takich zapytań: np. `<Fetch path="/foo/bar" method="post" />` albo bardziej generycznie `<Async fn={fetchStuff} params={{id:1}}>`. Ogólnie mistrzostwem świata jest to co ApolloGraphql zrobili ze swoim komponentem `Query`. To jest coś pięknego. Deklaratywne, uniwersalne rozwiązanie: https://www.apollographql.com/docs/react/essentials/queries.html Oczywiście to rozwiązanie jest nie dla nas na tem moment bo firma by pewnie eksplodowała.

---

## Requirements

Aby rozpocząć pracę potrzebne są dwie rzeczy:

- Manager pakietów [Yarn](http://yarnpkg.com)
- [NodeJS](http://nodejs.org) 

Oba warto zainstalować w aktualnej wersji. Nie będziemy przypinać się na sto lat do LTS czy coś w tym stylu.

## Start

Projekt podzielony jest na dwa repozytoria.

**UI**

To jest nasza biblioteka. Tutaj umieszczamy wszystkie komponenty niebiznesowe. Zdanie to będzie tworzyć trochę bo czasami ciężko jest określić czy dany komponent powinien być umieszczony w bibliotece czy aplikacji. Na pewno będą tam takie komponenty tak: `Box`, `Text`, `Button` , `Tooltiop` `Loader`, `Icon`, trochę wizualnych dodatków zgrupowanych np. w `Figures`, komponent odpowiedzialny za `theme` aplikacji. Naturalnie praca nad ujednoliceniem elementów pozwoli nam wyciągać tutaj też większe komponenty, np. `Calendar`, `Accordion` czy `Carousel`.

Głównym brakiem UI na tym etapie jest brak narzędzia do tworzenia styleguide + dokumentacji w ładny i łatwy sposób. Ten temat jest eksplorowany.

- [React Storybook](https://github.com/storybooks/storybook) - dużo trzeba się napisać aby utworzyć stronę + nie wiem jak wsparcie TS
- [React Styleguidist](https://github.com/styleguidist/react-styleguidist) - używałem tego z TS w poprzedniej firmie i było okej
- [Docz](https://docz.site) - super rozwiązanie pozwalające na tworzenie dokumentacji w formacie `MDX`. Obecnie jedyny minus to konieczność resetowania jego styli (przykłady nie są umieszczane w iframe).
- [Gatsby](https://gatsbyjs.org) - razem w pluginem `MDX` można stworzyć dowolną stronę.

**Portal** 

To jest nasza strona www. Istotne są tutaj dwa katalogi `pages` i `components`. Pomijając warstwę danych tutaj będziemy wszystko umieszczać. Pliki w katalogu pages będą mapowane na odpowiednie adresy URL. W katalogu components w rozsądny sposób możemy przechowywać specyficzne dla danej strony (grupy stron) komponenty, które w naszym odczuciu nie powinny znajdować się w bibliotece (np. jakaś strona będzie potrzebowała wykresu, albo infinite scroll grid).

## Development

Starałem się maksymalnie ujednolicić pracę z UI i Portalem tak aby start był szybki. Rozpoczęcie developmentu zawsze ogranicza się do uruchomienia w danym `package` komendy `yarn dev`. W przypadku biblioteki uruchomiony zostanie proces kompilacji TypeScript do formatu `commonjs` oraz `es`, generowane będą również pliki definicji `d.ts`. W przypadku Portalu `yarn dev` uruchomi server developerski na porcie `3000`. 

Wszystkie możliwe komendy dla `ui` i `portalu` można znaleźć w odpowiednich plikach `package.json`.

Stworzenie na sucho w 100% prawidłowych skryptów jest trudne tak więc mogą pojawić się jakieś problemy. Spełnione są jednak podstawowe wymagania.

Oczywiście development biblioteki i portalu jest mocno połączony. Aby całość wyglądała maksymalnie jak rozwój jednej aplikacji oba projekty warto zainstalować w odpowiedni sposób.

- tworzymy katalog projektu np. `orange.pl`

- umieszczamy w nim plik package.json

```
{
  "name": "orange.pl",
  "version": "1.0.0",
  "workspaces": [
    "packages/portal",
    "packages/ui"
  ],
  "private": true
}
```

Następnie do katalogów `packages/portal` oraz `packages/ui` instalujemy projekty. Yarn workspace to `npm link` na sterydach.  

## Uruchomienie portalu w wersji prod*

Przechodzimy do katalogu zawierającego `portal` i wykonujemy komendę `yarn build`. Odpalenie projektu to `yarn start` (również na porcie 3000). Budowanie produkcyjne pozwala nam również analizować jak została zbudowana aplikacja. Wykonując komendę `yarn analyze` dostaniemy `treemap` dla wszystkich paczek ([więcej informacji tutaj](https://github.com/webpack-contrib/webpack-bundle-analyzer)).

W katalogu portalu znajdziecie też plik `Caddyfile` , który pozwala na uruchomienie przykładowego serwera produkcyjnego (port 3001). Jest to w dużej mierze proxy + gzip + https (self signed) dla komendy `yarn start`. Więcej o serwerze Caddy  można poczytać [na ich stronie](https://caddyserver.com).

`*` - coś jeszcze może nie być w 100% optymalnie skompilowane

## Dodatkowe informacje

Ponieważ pakiety npm muszą mieć jakąś nazwę ustawiłem je na `@opl/portal` oraz `@opl/ui`. Jest to wymagane aby yarn prawidłowo linkował się w workspace.

Oba projektu używają tego samego setu narzędzi:

- typescript
- prettier
- TSLint

Są one skonfigurowane i jedyne czego mogą wymagać to ewentualnej integracji z waszym edytorem / IDE. Jeżeli ktoś szuka czegoś stabilnego, estetycznego i produktywnego totalnie polecam [Visual Studio Code](https://code.visualstudio.com/) (nie mylić z VisualStudio).

Czasami potrzebne jest sprawdzenie czegoś / hacking na szybko nowej rzeczy itp. Polecam kilka rozwiązań:

- [codesandbox.io](https://codesandbox.io) - do hackingów online
- [Parcel Bundler](https://parceljs.org/) - do hackingów lokalnie

Inne przydatne narzędzia

- [BundlePhobia](https://bundlephobia.com) - do sprawdzenia czy dany pakiet nie jest za duży do umieszczenia w projekcie

## Nad czym jeszcze będziemy pracować

1. formatowanie plików przed commitem
2. walidacja kodu `TS` przed commitem
3. automatyczne generowanie changelogu na podstawie commitów
4. być może warto przeprowadzić R&D dotyczące użycia projektu [Lerna](https://github.com/lerna/lerna/) jako wysokopoziomowej nakładki na `yarn workspace`.