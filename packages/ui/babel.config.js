module.exports = api => {
  const isProd = api.cache(() => process.env.NODE_ENV === 'production');

  const modulesType = modulesTypeFromTarget[process.env.BUILD_TARGET || 'es'];

  if (modulesType === undefined) {
    throw `Unknown module type for "${process.env.BUILD_TARGET}" target.`;
  }

  const presets = [
    ['@babel/preset-env', { debug: !isProd, modules: modulesType }],
    '@babel/preset-typescript',
    '@babel/preset-react'
  ];
  const plugins = [
    ['emotion', { hoist: isProd, sourceMap: !isProd, autoLabel: !isProd }],
    '@babel/plugin-proposal-class-properties',
    ['@babel/plugin-transform-runtime', { helpers: true }]
  ];

  return {
    presets,
    plugins
  };
};

const modulesTypeFromTarget = {
  cjs: 'cjs',
  es: false
};
