// @ts-ignore
import tag from 'clean-tag';
import styled from 'react-emotion';
import {
  alignContent,
  AlignContentProps,
  alignItems,
  AlignItemsProps,
  alignSelf,
  AlignSelfProps,
  border,
  BorderProps,
  borders,
  BordersProps,
  color,
  ColorProps,
  display,
  DisplayProps,
  flex,
  flexDirection,
  FlexDirectionProps,
  FlexProps,
  flexWrap,
  FlexWrapProps,
  justifyContent,
  JustifyContentProps,
  maxWidth,
  MaxWidthProps,
  minWidth,
  MinWidthProps
} from 'styled-system';

import {
  height,
  IHeightProps,
  ISpaceProps,
  IWidthProps,
  space,
  width
} from './styles';

type IProps = FlexProps &
  FlexDirectionProps &
  FlexWrapProps &
  JustifyContentProps &
  AlignContentProps &
  AlignItemsProps &
  AlignSelfProps &
  DisplayProps &
  ISpaceProps &
  ColorProps &
  IWidthProps &
  IHeightProps &
  MaxWidthProps &
  MinWidthProps &
  BorderProps &
  BordersProps & {
    is?: string;
  };

export const Box = styled(tag.div as React.ComponentType)<IProps>`
  ${flex};
  ${flexWrap};
  ${flexDirection};
  ${justifyContent};
  ${alignContent};
  ${alignItems};
  ${alignSelf};
  ${display};
  ${space};
  ${color};
  ${height};
  ${width};
  ${maxWidth};
  ${minWidth};
  ${border};
  ${borders};
`;
