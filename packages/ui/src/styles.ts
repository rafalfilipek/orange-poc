/**
 * `styled-system` dostarcza funkcje do kontroli zaimplementowanych tutaj reguł CSS.
 * Zgodnie z dokumentacją tworząc style responsywne użytkownik musi utworzyć tablicę
 * której kolejne wartości odpowiadają indeksom w opcji konfiguracyjnej scale
 * dla danej właściwości. Dla domyślnych ustawień pojawia się przykładow mapowanie:
 * `3 => 8`, `5 => 32`.
 *
 * Rozwiązanie to mentalnie utrudnia szybkie mapowanie wartości dostarczanych na
 * wyjściowe.
 *
 * Celem funkcji zdefiniowanych w tym pliki jest pozwolenie użytkownikowi na:
 *
 *  * podwania jesno określonych, typowanych wartości
 *  * bezpośrednie odniesienie do bazowej wartości `16`
 *  * domyślne definiowanie wartości przy użyciu jednostek `rem`
 *
 * Dzięki temu możliwy jest zapis:
 *
 *    <Box m={[0.25, 0.5, 1, 1.5]} />
 *
 * Obecnie zdefiniowane są funkcje dla:
 *
 *  * `padding` (bez `px`, `py`)
 *  * `margin` (bez `mx`, `my`)
 *  * `fontSize`
 *  * `width` (jako `w`)
 *  * `height` (jako `h`)
 */

import { style } from 'styled-system';

const transformValue = (n: unknown): number | string | null => {
  if (typeof n === 'string') {
    return n;
  }
  if (typeof n === 'number') {
    return `${n}rem;`;
  }
  return null;
};

const createStyle = (attrs: { prop: string; cssProp?: string; key?: string }) =>
  style({
    prop: attrs.prop,
    cssProperty: attrs.cssProp || attrs.prop,
    key: attrs.key,
    transformValue
  });

type IValues = string | 0 | 0.25 | 0.5 | 0.75 | 1 | 1.5 | 2 | 2.5 | 3 | 4;

export interface IFontSizeProps {
  fontSize?: IValues | IValues[];
}

export interface IWidthProps {
  w?: IValues | IValues[];
}

export interface IHeightProps {
  h?: IValues | IValues[];
}

export interface ISpaceProps {
  p?: IValues | IValues[];
  m?: IValues | IValues[];
  pt?: IValues | IValues[];
  pr?: IValues | IValues[];
  pb?: IValues | IValues[];
  pl?: IValues | IValues[];
  mt?: IValues | IValues[];
  mr?: IValues | IValues[];
  mb?: IValues | IValues[];
  ml?: IValues | IValues[];
}

const spaceNamesMap: { [key: string]: string } = {
  p: 'padding',
  m: 'margin',
  t: 'Top',
  r: 'Right',
  b: 'Bottom',
  l: 'Left'
};

const spaces = ['p', 'm', 'pt', 'pr', 'pb', 'pl', 'mt', 'mr', 'mb', 'ml'];

const spaceStyles: { [key: string]: typeof style } = spaces.reduce(
  (memo: { [key: string]: any }, prop) => {
    const cssProp = prop
      .split('')
      .map(letter => spaceNamesMap[letter])
      .join('');
    memo[prop] = createStyle({ prop, cssProp });
    return memo;
  },
  {}
);

export const fontSize = createStyle({ prop: 'fontSize' });
export const width = createStyle({ prop: 'w', cssProp: 'width' });
export const height = createStyle({ prop: 'h', cssProp: 'height' });
export const space = (props: any) => {
  return spaces.reduce((memo: any, el) => {
    if (props[el] !== undefined) {
      memo = { ...memo, ...spaceStyles[el](props) };
    }
    return memo;
  }, {});
};
