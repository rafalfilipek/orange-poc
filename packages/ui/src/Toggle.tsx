import Component from '@reach/component-component';
import React from 'react';

import { Box } from './Box';

interface IChildrenProps {
  isOn: boolean;
  toggle(): void;
}

interface IProps {
  children(props: IChildrenProps): React.ReactChild;
}

export const Toggle: React.SFC<IProps> = ({ children }) => (
  <Box>
    <Component<{ isOn: boolean }>
      initialState={{
        isOn: false
      }}
    >
      {({ state, setState }: any) =>
        children({
          isOn: state.isOn,
          toggle: () => setState({ isOn: !state.isOn })
        })
      }
    </Component>
  </Box>
);
