// @ts-ignore
import Rect from '@reach/rect';
import React from 'react';
// @ts-ignore
import Swipe from 'react-easy-swipe';
import { css } from 'react-emotion';
import { animated } from 'react-spring';

import { Scroller } from './Scroller';

const containerClass = css`
  overflow: hidden;
`;

const scrollContainerClass = css`
  display: flex;
  user-select: none;
  will-change: transform;
  touch-action: pan-y;
`;

interface IProps {
  count: number;
  responsive?: boolean;
  show: (width: string | number) => number;
  children: (props: { visibleIndexes: number[] }) => JSX.Element[];
}

interface IState {
  count: number;
  show: number;
  rect: {
    width: string | number;
  };
}

export class Carousel extends React.PureComponent<IProps, IState> {
  static defaultProps = {
    responsive: true
  };

  state = {
    count: this.props.count,
    show: this.props.show('100%'),
    rect: { width: '100%' }
  };

  getElementWidth = (): string | number => {
    /**
     * Gdy renderowanie odbywa się po stronie serwera nie znamy szerokości
     * kontenera. Dlatego przyjmujemy jego szerokość jako 100%.
     * W tym momencie możemy przedstawić szerokość elementów procentowo.
     */
    if (typeof this.state.rect.width === 'string') {
      return 100 / this.state.show + '%';
    }
    return this.state.rect.width / this.state.show;
  };

  onResize = (rect: { width: number }) => {
    requestAnimationFrame(() => {
      this.setState({
        rect,
        show: this.props.show(rect.width)
      });
    });
  };

  get shouldObserveWidth() {
    if (this.props.responsive) {
      return true;
    }
    if (!this.props.responsive && this.state.rect) {
      return false;
    }
    return true;
  }

  render() {
    const elementWidth = this.getElementWidth();

    return (
      <Rect observe={this.shouldObserveWidth} onChange={this.onResize}>
        {({ ref }: { ref: React.Ref<any> }) => (
          <div>
            <Scroller
              visibleElementsCount={this.state.show}
              elementsCount={this.state.count}
              elementWidth={elementWidth}
              width={this.state.rect.width}
            >
              {({ visibleIndexes }) => style => (
                <div className={containerClass} ref={ref}>
                  {/*
                    Dzięki `noscript` możemy odblokować scroll w kontenerze
                    co pozwala na normalne działanie bez JS.
                  */}
                  <noscript>
                    <style>{`.${containerClass} { overflow: scroll }`}</style>
                  </noscript>
                  <animated.div className={scrollContainerClass} style={style}>
                    <>
                      {React.Children.map(
                        this.props.children({ visibleIndexes }),
                        child => {
                          /**
                           * Nie wiem czemu to typowanie rzuca błąd.
                           * Każdy element karuzeli to komponent.
                           */
                          // @ts-ignore
                          return React.cloneElement(child, {
                            style: {
                              width: elementWidth
                            }
                          });
                        }
                      )}
                    </>
                  </animated.div>
                </div>
              )}
            </Scroller>
          </div>
        )}
      </Rect>
    );
  }
}
