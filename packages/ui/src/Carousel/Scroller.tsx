import React from 'react';
// @ts-ignore
import Swipe from 'react-easy-swipe';
import { Spring, SpringRendererFunc } from 'react-spring';

interface IProps {
  elementsCount: number;
  elementWidth: number | string;
  visibleElementsCount: number;
  width: number | string;
  children: (
    props: {
      visibleIndexes: number[];
      goNext: () => void;
      goPrev: () => void;
    }
  ) => SpringRendererFunc<any, any>;
}

interface IState {
  x: number;
  index: number;
  type: 'left' | 'right' | null;
  animate: boolean;
}

export class Scroller extends React.PureComponent<IProps, IState> {
  state = {
    x: 0,
    index: 0,
    type: null,
    animate: false
  };

  componentDidUpdate(prevProps: IProps) {
    if (
      prevProps.width === this.props.width ||
      typeof prevProps.width === 'string'
    ) {
      return;
    }

    const index = Math.max(
      Math.min(
        this.state.index,
        this.props.elementsCount - this.props.visibleElementsCount
      ),
      0
    );
    this.goToIndex(index);
  }

  get baseX() {
    const { elementWidth } = this.props;
    return (
      -1 *
      this.state.index *
      (typeof elementWidth === 'string' ? 0 : elementWidth)
    );
  }

  start = () => {
    this.setState({ animate: false });
  };

  move = (position: { x: number }) => {
    this.setState({
      x: this.baseX + position.x,
      type: position.x > 0 ? 'left' : 'right'
    });
  };

  end = () => {
    let index = this.state.index;

    if (this.state.type === 'left') {
      index = this.calcucateLeftIndex();
    }

    if (this.state.type === 'right') {
      index = this.calculateRightIndex();
    }

    this.goToIndex(index);
  };

  goToIndex = (index: number) => {
    if (typeof this.props.elementWidth === 'string') {
      return;
    }
    const v = -1 * index * this.props.elementWidth;

    this.setState({ index, x: v, animate: true });
  };

  calcucateLeftIndex = () => {
    if (typeof this.props.elementWidth === 'string') {
      return this.state.index;
    }

    const v = Math.min(0, this.state.x);
    return Math.abs(Math.ceil(v / this.props.elementWidth));
  };

  calculateRightIndex = () => {
    if (typeof this.props.elementWidth === 'string') {
      return this.state.index;
    }

    const { elementsCount, visibleElementsCount, elementWidth } = this.props;

    const invisibleElementsCount = elementsCount - visibleElementsCount;
    const maxTranslate = invisibleElementsCount * elementWidth;
    const v = -Math.min(Math.abs(this.state.x), maxTranslate);
    return Math.abs(Math.floor(v / elementWidth));
  };

  get visibleIndexes() {
    return range(
      this.state.index,
      this.state.index + this.props.visibleElementsCount
    );
  }

  goPrev = () => {
    this.goToIndex(Math.max(this.state.index - 1, 0));
  };

  goNext = () => {
    this.goToIndex(
      Math.min(
        this.state.index + 1,
        this.props.elementsCount - this.props.visibleElementsCount
      )
    );
  };

  render() {
    return (
      <div>
        <Swipe
          allowMouseEvents
          onSwipeStart={this.start}
          onSwipeMove={this.move}
          onSwipeEnd={this.end}
        >
          <Spring
            native
            immediate={!this.state.animate}
            from={{ transform: `translateX(${this.baseX}px)` }}
            to={{ transform: `translateX(${this.state.x}px)` }}
          >
            {this.props.children({
              visibleIndexes: this.visibleIndexes,
              goNext: this.goNext,
              goPrev: this.goPrev
            })}
          </Spring>
        </Swipe>
      </div>
    );
  }
}

function range(from: number, to: number) {
  return new Array(to - from).fill(from).map<number>((v, index) => v + index);
}
