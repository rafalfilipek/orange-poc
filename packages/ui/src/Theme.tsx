import { ThemeProvider } from 'emotion-theming';
import React from 'react';

type IBasicColors =
  | 'white'
  | 'grayLight'
  | 'orangeDark'
  | 'orangeLight'
  | 'grayOBS'
  | 'black';

type ISupportColors = 'yellow' | 'blue' | 'green' | 'purple' | 'pink';

type IGrayColors = 'e' | 'd' | 'c' | '9' | '6' | '4' | '3' | '23';

interface IColors {
  basic: { [K in IBasicColors]: string };
  support: { [K in ISupportColors]: string };
  gray: { [K in IGrayColors]: string };
}

const colors: IColors = {
  basic: {
    white: '#fff',
    grayLight: '#f5f5f5',
    orangeDark: '#f16e00',
    orangeLight: '#ff7900',
    grayOBS: '#595959',
    black: '#000'
  },
  support: {
    yellow: '#ffd200',
    blue: '#4bb4e6',
    green: '#50be87',
    purple: '#a885d8',
    pink: '#ffb4e6'
  },
  gray: {
    e: '#eee',
    d: '#ddd',
    c: '#ccc',
    9: '#099',
    6: '#666',
    4: '#444',
    3: '#333',
    23: '#232323'
  }
};

interface ISpaces {
  [key: number]: number;
  xs: number;
  s: number;
  m: number;
  l: number;
  xl: number;
  xxl: number;
  xxxl: number;
}

// @ts-ignore
const space: ISpaces = [0, 4, 8, 12, 16, 24, 32, 48, 64];
space.xs = 4;
space.s = 8;
space.m = 16;
space.l = 24;
space.xl = 32;
space.xxl = 48;
space.xxxl = 64;

interface IBreakpoints {
  [key: number]: string;
  s: string;
  m: string;
  l: string;
}

// @ts-ignore
const breakpoints: IBreakpoints = ['40rem', '52rem', '64rem'];
breakpoints.s = '40rem';
breakpoints.m = '52rem';
breakpoints.l = '64rem';

export interface ITheme {
  colors: IColors;
  breakpoints: IBreakpoints;
  space: ISpaces;
}

const theme: ITheme = {
  colors,
  breakpoints,
  space
};

export const Theme: React.SFC = ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
