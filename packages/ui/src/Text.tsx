// @ts-ignore
import tag from 'clean-tag';
import styled from 'react-emotion';
import {
  color,
  ColorProps,
  display,
  DisplayProps,
  fontFamily,
  FontFamilyProps,
  fontWeight,
  FontWeightProps,
  lineHeight,
  LineHeightProps
} from 'styled-system';

import {
  fontSize,
  height,
  IFontSizeProps,
  IHeightProps,
  ISpaceProps,
  IWidthProps,
  space,
  width
} from './styles';

type IProps = ColorProps &
  DisplayProps &
  FontFamilyProps &
  IFontSizeProps &
  FontWeightProps &
  LineHeightProps &
  ISpaceProps &
  IWidthProps &
  IHeightProps & {
    is?: string;
  };

export const Text = styled(tag.div as React.ComponentType)<IProps>`
  ${fontFamily};
  ${fontSize};
  ${fontWeight};
  ${display};
  ${space};
  ${color};
  ${width};
  ${height};
  ${lineHeight};
`;
