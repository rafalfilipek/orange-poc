declare module '@reach/component-component' {
  import * as React from 'react';

  interface IChildrenProps<T> {
    state: T;
    setState: (newState: Partial<T>) => void;
  }

  interface IProps<T extends any> {
    initialState?: T;
    getInitialState?(): T;
    children(props: IChildrenProps<T>): void;
  }

  export default class Component<T> extends React.Component<IProps<T>> {}
}
