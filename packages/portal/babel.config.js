module.exports = api => {
  const isProd = api.cache(() => process.env.NODE_ENV === 'production');

  const presets = [
    ['@babel/preset-env', { debug: !isProd, useBuiltIns: 'usage' }],
    '@zeit/next-typescript/babel',
    'next/babel'
  ];

  const plugins = [
    ['emotion', { hoist: isProd, sourceMap: !isProd, autoLabel: !isProd }]
  ];

  return {
    presets,
    plugins
  };
};
