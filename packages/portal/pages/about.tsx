import { Box, Toggle } from '@opl/ui';

export default () => (
  <Box color="red" p="20">
    <Toggle>
      {({ isOn, toggle }) => (
        <button onClick={toggle}>{isOn ? 'tak' : 'nie'}</button>
      )}
    </Toggle>
  </Box>
);
