export default () => <div>test</div>;

// import { css } from 'emotion';
// import React from 'react';

// import { Box } from '@opl/ui';

// const elementClass = css`
//   height: 100px;
// `;

// export default () => {
//   return (
//     <Scroller
//       width={600}
//       elementWidth={200}
//       elementsCount={3}
//       visibleElementsCount={3}
//       infinite
//     >
//       <Box bg="red" w="200px" h="200px">
//         1
//       </Box>
//       <Box bg="green" w="200px" h="200px">
//         2
//       </Box>
//       <Box bg="blue" w="200px" h="200px">
//         3
//       </Box>
//     </Scroller>
//   );
// };

// interface IScrollerProps {
//   elementWidth: number;
//   width: number;
//   elementsCount: number;
//   visibleElementsCount: number;
//   infinite: boolean;
// }

// function getInitialState(props) {

// }

// class Scroller extends React.Component<IScrollerProps> {
//   state = getInitialState(this.props);

//   render() {
//     return (
//       <Box w={this.props.width + 'px'} bg="red" display="flex">
//         {this.props.children}
//       </Box>
//     );
//   }
// }

// /*
// <Carousel>
//       {() =>
//         [1, 2, 3].map(i => (
//           <div key={i} className={elementClass}>
//             {i}
//           </div>
//         ))
//       }
//     </Carousel> */

// interface ICarouselProps {
//   children: () => Array<React.ReactElement<any>>;
// }

// class Carousel extends React.PureComponent<ICarouselProps> {
//   render() {
//     return this.props.children();
//   }
// }
