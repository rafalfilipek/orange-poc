import App, { Container, NextAppContext } from 'next/app';
import Head from 'next/head';
import React from 'react';

import { Theme } from '@opl/ui';

interface IProps {
  isServer: boolean;
  pageProps: any;
}

export default class MyApp extends App<IProps> {
  // @ts-ignore TODO typowanie
  static async getInitialProps({ Component, ctx }: NextAppContext) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return {
      isServer: !!ctx.req,
      pageProps
    };
  }

  render() {
    const { Component, pageProps, isServer } = this.props;
    return (
      <Theme>
        <Container>
          <Head>
            <title>Orange.pl</title>
          </Head>
          <Component {...pageProps} isServer={isServer} />
        </Container>
      </Theme>
    );
  }
}
