// import Link from 'next/link';
import Head from 'next/head';
import React from 'react';
import styled from 'react-emotion';

import { Box, Carousel, Text } from '@opl/ui';

import { fetchPageConfig } from '../api/page';
import { IPage } from '../types/page';

import { Glorius } from '../components/Glorius';
import { Header } from '../components/Header';
import { Nba } from '../components/Nba';

type IProps = IPage & { error?: Error };
export default class Index extends React.Component<IProps> {
  static async getInitialProps() {
    try {
      const config = await fetchPageConfig('index');
      return config;
    } catch (error) {
      return {
        error: {
          message: error.message
        }
      };
    }
  }

  render() {
    const { header, title, description, keywords, error } = this.props;

    if (error) {
      return <div>error: {error.message}</div>;
    }

    return (
      <>
        <Head>
          {title && <title>{title}</title>}
          {description && <meta name="description" content={description} />}
          {keywords && <meta name="keywords" content={keywords.join(', ')} />}
        </Head>
        <Header topLinks={header.topLinks} menu={header.menu} />
        <Box maxWidth={1300} m="0 auto">
          <Text bg="red" w={['10rem', '20rem']} m={[2, 4]}>
            omfg
          </Text>
          <Nba />
          <Box mt="1.5rem">
            <Glorius />
          </Box>
          <Box mt="1.5rem">
            <Carousel count={data.length} show={() => 1}>
              {({ visibleIndexes }) =>
                data.map((el, i) => (
                  <Element key={i} active={isActiveIndex(visibleIndexes, i)}>
                    <img src={base + el.img} alt="" />
                    <strong>{el.title}</strong>
                    <span dangerouslySetInnerHTML={{ __html: el.subtitle }} />
                  </Element>
                ))
              }
            </Carousel>
          </Box>
        </Box>
      </>
    );
  }
}

const data = [
  {
    img: 'p20.png',
    title: '119 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'sam_gal_a6.png',
    title: '0 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'G7.png',
    title: '0 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'xz3.png',
    title: '2 - 99 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'p20.png',
    title: '2 - 119 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'sam_gal_a6.png',
    title: '2 - 0 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'G7.png',
    title: '2 - 0 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  },
  {
    img: 'xz3.png',
    title: '99 zł na start',
    subtitle: 'Huawei P20 128GB<br />Dual SIM'
  }
];

const base =
  'https://orange.binaries.pl/binaries/o/map/ak/html/strony_dedykowany/CP_winter_telefony_sg/';

const isActiveIndex = (visibleIndexes: number[], index: number) => {
  return true;
  if (visibleIndexes.length === 1) {
    return visibleIndexes[0] === index || undefined;
  }
  if (visibleIndexes.length === 3) {
    return visibleIndexes[1] === index || undefined;
  }
  return undefined;
};

const Element = styled(Box)<{ active?: boolean }>`
  flex-shrink: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 1rem;
  font-weight: 600;
  color: #fff;
  padding: 16px;
  user-select: none;
  background: #fff;

  img {
    transition: transform 0.1s ease-out 0.5s, opacity 0.1s ease-out 0.5s;
    opacity: ${props => (props.active ? 1 : 0.5)};
    transform: ${props => (props.active ? 'scale(1.1)' : undefined)};
    pointer-events: none;
  }
`;
