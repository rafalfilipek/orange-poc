import { injectGlobal } from 'emotion';
import { extractCritical } from 'emotion-server';
import Document, {
  DocumentProps,
  Head,
  Main,
  NextDocumentContext,
  NextScript
} from 'next/document';

// tslint:disable-next-line
injectGlobal`
  *, *::before, *::after {
    box-sizing: border-box;
  }

  html, body {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", HelveticaNeue, Arial, Helvetica, sans-serif;
    font-size: 16px;
  }
`;

type IProps = DocumentProps & {
  css?: string;
};

export default class MyDocument extends Document<IProps> {
  static async getInitialProps(ctx: NextDocumentContext) {
    const page = ctx.renderPage();
    const styles = extractCritical(page.html as string);
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps, ...page, ...styles };
  }

  constructor(props: DocumentProps & { ids: string[] }) {
    super(props);
    const { __NEXT_DATA__, ids } = props;
    if (ids) {
      // @ts-ignore
      __NEXT_DATA__.ids = ids;
    }
  }

  render() {
    return (
      <html>
        <Head>
          <link rel="dns-prefetch" href="https://www.orange.pl" />
          <link rel="dns-prefetch" href="https://use.fontawesome.com" />
          <link ref="dns-prefetch" href="https://orange.binaries.pl" />
          <style dangerouslySetInnerHTML={{ __html: this.props.css || '' }} />
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
            integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz"
            crossOrigin="anonymous"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, viewport-fit=cover"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
