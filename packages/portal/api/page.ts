import { client } from './client';

interface IPageResponse {
  header: {
    topLinks: {
      left: Array<{ name: string; route: string; active: boolean }>;
      right: Array<{ name: string; route: string }>;
    };
    menu: any;
  };
  footer: any;
}

export const fetchPageConfig = async (id: string) => {
  try {
    const r = await client.get<IPageResponse>('/page', { params: { id } });
    return r.data;
  } catch (e) {
    throw e;
  }
};
