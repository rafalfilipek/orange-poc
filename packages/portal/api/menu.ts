import { client } from './client';

interface IMenuResponse {
  topLinks: Array<{ name: string; route: string }>;
  logo?: string;
}

export interface IMenuResult {
  data?: IMenuResponse;
  error?: Error;
}

export const fetchMenu = async (): Promise<IMenuResult> => {
  try {
    const r = await client.get<IMenuResponse>('');
    return { data: r.data };
  } catch (error) {
    return { error };
  }
};
