import axios from 'axios';

export const client = axios.create({
  timeout: 1000,
  baseURL: 'https://mmomqnmp08.sse.codesandbox.io'
});
