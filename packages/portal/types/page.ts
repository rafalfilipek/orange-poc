export interface ITopLinksLink {
  name: string;
  route: string;
  active?: boolean;
}

export interface IHeaderTopLinks {
  left: ITopLinksLink[];
  right: ITopLinksLink[];
}

export interface IMenuElement {
  name: string;
}

export interface IPageHeader {
  topLinks: IHeaderTopLinks;
  menu: {
    elements: IMenuElement[];
    logo?: string;
  };
}

export interface IPage {
  header: IPageHeader;
  footer: any;
  title?: string;
  description?: string;
  keywords?: string[];
}
