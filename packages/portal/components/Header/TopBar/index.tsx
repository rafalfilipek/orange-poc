import React from 'react';

import { Box } from '@opl/ui';

import { IHeaderTopLinks } from '../../../types/page';

import { Link } from './Link';

interface IProps {
  leftElements: IHeaderTopLinks['left'];
  rightElements: IHeaderTopLinks['right'];
}

export class TopBar extends React.Component<IProps> {
  render() {
    const { leftElements, rightElements } = this.props;

    return (
      <Box bg="#222">
        <Box maxWidth={1300} display="flex" m="0 auto">
          <Box flex="1">
            {leftElements.map(element => (
              <Link
                key={element.route}
                href={element.route}
                active={element.active}
              >
                {element.name}
              </Link>
            ))}
          </Box>
          <Box>
            <Link href="/kontakt">Kontakt</Link>
            {rightElements.map(element => (
              <Link key={element.route} href={element.route}>
                {element.name}
              </Link>
            ))}
          </Box>
        </Box>
      </Box>
    );
  }
}
