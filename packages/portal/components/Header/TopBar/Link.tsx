import NextLink from 'next/link';
import React from 'react';

import styled from 'react-emotion';

const A = styled.a<{ active?: boolean }>(
  {
    fontSize: 12,
    padding: '0.5rem 0.75rem',
    display: 'inline-block',
    textDecoration: 'none',
    ':hover': {
      color: '#ff7900'
    }
  },
  props => ({
    color: props.active ? '#ff7900' : '#999',
    backgroundColor: props.active ? '#000' : '#232323'
  })
);

interface IProps {
  href: string;
  active?: boolean;
}

export const Link: React.SFC<IProps> = ({ children, active, href }) => (
  <NextLink href={href} passHref>
    <A active={active}>{children}</A>
  </NextLink>
);
