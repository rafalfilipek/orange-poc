import React from 'react';

import {} from '@opl/ui';

import { IPageHeader } from '../../types/page';

import { Menu } from './Menu';
import { TopBar } from './TopBar';

export class Header extends React.Component<IPageHeader> {
  render() {
    const { topLinks, menu } = this.props;

    return (
      <>
        <TopBar leftElements={topLinks.left} rightElements={topLinks.right} />
        <Menu elements={menu.elements} logo={menu.logo} />
      </>
    );
  }
}
