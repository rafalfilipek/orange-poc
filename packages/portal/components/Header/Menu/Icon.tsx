import React from 'react';

export const Icon: React.SFC<{ name: string }> = ({ name }) => (
  <i
    className={`fas fa-${name}`}
    css={`
      font-size: 1.5rem;
      &:hoverx {
        color: #ff7900;
      }
    `}
  />
);
