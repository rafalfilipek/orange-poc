import React from 'react';

import { Box } from '@opl/ui';
import { Elememt } from './Elememt';
import { Icon } from './Icon';

import { IMenuElement } from '../../../types/page';

interface IProps {
  elements: IMenuElement[];
  logo?: string;
}

export class Menu extends React.Component<IProps> {
  render() {
    const { elements, logo } = this.props;

    return (
      <Box bg="#000">
        <Box maxWidth={1300} display="flex" m="0 auto" p="1rem 0" pl="0.5rem">
          <Box flex={1} display="flex">
            <img
              src={
                logo ||
                'https://www.orange.pl/medias/sys_master/images/images/h9b/h7a/8888162811934/logo-orange-65Wx65H.png'
              }
              alt=""
              width="50"
              height="50"
            />
            {elements.map(element => (
              <Elememt>{element.name}</Elememt>
            ))}
          </Box>
          <Box display="flex">
            <Elememt active>
              <Icon name="search" />
            </Elememt>
            <Elememt>
              <Icon name="shopping-basket" />
            </Elememt>
            <Elememt>
              <Icon name="user" />
            </Elememt>
          </Box>
        </Box>
      </Box>
    );
  }
}
