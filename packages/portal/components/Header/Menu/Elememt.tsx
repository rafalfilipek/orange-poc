import { Box, Text } from '@opl/ui';
import React from 'react';

export const Elememt: React.SFC<{ active?: boolean }> = ({
  children,
  active
}) => (
  <Box display="flex" alignItems="center" p="0 1.5rem">
    <Text fontWeight={600} color={active ? '#ff7900' : '#fff'}>
      {children}
    </Text>
  </Box>
);
