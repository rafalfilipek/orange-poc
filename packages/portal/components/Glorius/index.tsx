import React from 'react';
import styled from 'react-emotion';

import { Box, Text } from '@opl/ui';

const Panel = styled(Box)({
  boxShadow: '0 2px 5px rgba(0,0,0,.2)',
  ':hover': {
    boxShadow: '0 5px 20px rgba(0,0,0,.2)'
  }
});

export class Glorius extends React.Component {
  render() {
    return (
      <Box display="flex">
        <Panel flex={1} mr="0.5rem">
          panel
        </Panel>
        <Box flex={1} ml="0.5rem" flexDirection="column">
          <Panel mb="1rem">Top</Panel>
          <Box display="flex">
            <Panel flex={1} mr="0.5rem">
              left
            </Panel>
            <Panel flex={1} ml="0.5rem">
              <Text fontWeight={600}>
                Wakacje od rat.
                <br />
                <Text color="orange">Przez pół roku nie płacisz</Text>
                za wymarzony smartfon
              </Text>
              <img
                src="https://orange.binaries.pl/binaries/o/map/ak/html/strony_dedykowany/wakacjeodrat/banner/Popoutdesktop.jpg"
                alt=""
                width={259}
              />
            </Panel>
          </Box>
        </Box>
      </Box>
    );
  }
}
