import React from 'react';
import styled from 'react-emotion';

import { Box, Text } from '@opl/ui';

const Wrapper = styled(Box)({
  borderLeft: '5px solid #527edb',
  padding: '1.5rem',
  boxShadow: '0 2px 5px rgba(0,0,0,.2)',
  fontSize: '1.2rem',
  fontWeight: 600,
  alignItems: 'center',
  display: 'flex'
});

export class Nba extends React.Component {
  render() {
    return (
      <Wrapper>
        Sprawdź, jak prosto przenieść numer do Orange i
        <Text is="span" color="#ff7900">
          &nbsp;zyskać 3 mies. abonamentu za 0 zł
        </Text>
        <i
          className="fas fa-angle-right"
          css={`
            margin-left: 1rem;
          `}
        />
      </Wrapper>
    );
  }
}
